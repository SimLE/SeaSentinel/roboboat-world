import os
from glob import glob
from pathlib import Path

from setuptools import find_packages, setup

package_name = "roboboat_gazebo"

setup(
    name=package_name,
    version="0.0.0",
    packages=find_packages(),
    data_files=[
        ("share/" + package_name, ["package.xml"]),
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        (
            os.path.join("share", package_name, "launch"),
            glob(os.path.join("launch", "*launch.[pxy][yma]*")),
        ),
        (
            os.path.join("share", package_name, "launch"),
            ["launch/sitl_run.sh"],
        ),
        (
            os.path.join("share", package_name, "worlds"),
            glob(os.path.join("worlds", "*.world")),
        ),
        *[
            (str(Path("share") / package_name / p.parent), [str(p)])
            for p in Path("models").rglob("*.*")
        ],
    ],
    scripts=["launch/sitl_run.sh"],
    install_requires=["setuptools"],
    zip_safe=True,
    maintainer="user",
    maintainer_email="user@todo.todo",
    description="TODO: Package description",
    license="GPLv3",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [],
    },
)
