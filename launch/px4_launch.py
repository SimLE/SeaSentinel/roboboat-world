import os
from pathlib import Path

from ament_index_python.packages import get_package_share_directory
from launch_ros.actions import Node

from launch import LaunchDescription
from launch.actions import (
    DeclareLaunchArgument,
    ExecuteProcess,
    IncludeLaunchDescription,
)
from launch.launch_description_sources import PythonLaunchDescriptionSource
from launch.substitutions import (
    LaunchConfiguration,
    PathJoinSubstitution,
    TextSubstitution,
    ThisLaunchFileDir,
)

# PX4 is finicky, so we can't install it in colcon_ws
PX4_PATH = Path(os.getenv("PX4_PATH"))
if not PX4_PATH.exists():
    raise NotADirectoryError("Set PX4_PATH env pointing to PX4-Autopilot firmware")
PX4_TARGET = "px4_sitl_default"

px4_build_path = PX4_PATH / "build" / PX4_TARGET
px4_rootfs = px4_build_path / "rootfs"
px4_sitl_bin = px4_build_path / "bin" / "px4"
px4_gazebo = PX4_PATH / "Tools/simulation/gazebo-classic"


def generate_launch_description():
    package_name = "roboboat_gazebo"
    # set packages and files paths
    pkg_path = Path(get_package_share_directory(package_name))
    px4_path = str(PX4_PATH)

    # launch arguments
    spawn_x = 0
    spawn_y = 0
    spawn_z = 0
    spawn_R = 0
    spawn_P = 0
    spawn_Y = 0

    px4_command_arg1 = "-d"

    world_arg = DeclareLaunchArgument(
        "world",
        default_value=TextSubstitution(text="course_a"),
    )

    gui = "true"
    debug = "false"
    verbose = "false"
    paused = "false"
    respawn_gazebo = "false"

    # set pkg name
    vehicle = "uuv_bluerov2_heavy"
    sdf_arg = DeclareLaunchArgument(
        "vehicle_sdf",
        default_value=TextSubstitution(
            text=f"{px4_path}/Tools/simulation/gazebo-classic/sitl_gazebo-classic/models/{vehicle}/{vehicle}.sdf"
        ),
    )

    gazebo = IncludeLaunchDescription(
        PythonLaunchDescriptionSource(
            [str(pkg_path / "launch" / "gazebo_ros_launch.py")]
        ),
        launch_arguments={
            "world": LaunchConfiguration("world"),
        }.items(),
    )

    sitl = ExecuteProcess(
        additional_env={"PX4_SIM_MODEL": "gazebo-classic_usv_sese_rover"},
        cmd=[
            PathJoinSubstitution([ThisLaunchFileDir(), "sitl_run.sh"]),
            str(px4_sitl_bin),
            "",
            str(PX4_PATH),
            str(px4_build_path),
        ],
        output="screen",
    )

    spawn_vehicle = Node(
        package="gazebo_ros",
        executable="spawn_entity.py",
        arguments=[
            "-file",
            LaunchConfiguration("vehicle_sdf"),
            "-entity",
            "asv",
            "-x",
            str(spawn_x),
            "-y",
            str(spawn_y),
            "-z",
            str(spawn_z),
            "-R",
            str(spawn_R),
            "-P",
            str(spawn_P),
            "-Y",
            str(spawn_Y),
        ],
        output="screen",
    )

    # Launch them all!
    return LaunchDescription(
        [
            world_arg,
            sdf_arg,
            gazebo,
            sitl,
            spawn_vehicle,
        ]
    )
