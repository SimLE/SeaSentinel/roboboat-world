import os
from pathlib import Path

from ament_index_python.packages import get_package_share_directory

from launch import LaunchDescription
from launch.actions import DeclareLaunchArgument, ExecuteProcess
from launch.substitutions import (
    LaunchConfiguration,
    PathJoinSubstitution,
    PythonExpression,
    TextSubstitution,
)


def generate_launch_description():
    package_name = "roboboat_gazebo"

    # set packages and files paths
    pkg_share = Path(get_package_share_directory(package_name))
    pkg_world_path = pkg_share / "worlds"
    available_worlds = [w.stem for w in pkg_world_path.glob("*.world")]

    pkg_models_path = pkg_share / "models"
    gazebo_model_path = os.getenv("GAZEBO_MODEL_PATH").split(":")
    # We are adding path to models ad-hoc
    if str(pkg_models_path) not in gazebo_model_path:
        # raise ValueError(str(pkg_models_path) + ' not in GAZEBO_MODEL_PATH')
        gazebo_model_path.append(str(pkg_models_path))

    world_arg = DeclareLaunchArgument(
        "world",
        default_value=TextSubstitution(text="course_a"),
        choices=available_worlds,
    )

    gazebo = ExecuteProcess(
        additional_env={"GAZEBO_MODEL_PATH": ":".join(gazebo_model_path)},
        cmd=[
            "gazebo",
            "--verbose",
            PathJoinSubstitution(
                [
                    str(pkg_share),
                    "worlds",
                    PythonExpression(
                        ["'", LaunchConfiguration("world"), "' + '.world'"]
                    ),  # I must scream
                ]
            ),
            "-s",
            "libgazebo_ros_init.so",
            "-s",
            "libgazebo_ros_factory.so",
        ],
        output="screen",
    )

    return LaunchDescription(
        [
            world_arg,
            gazebo,
        ]
    )
