"""Generate Task 1, from RoboBoat 2023

Specs:
    - gap between gates 1.8m - 3m
    - distance between gates 7.5m - 30m
    - starting position, 1.8m away from first gate

Returns:
    Permutations of all spec boundaries, and worlds where [0,0] is ASV starting location
"""

import sys
import xml.etree.ElementTree as ET
from glob import glob
from pathlib import Path

import numpy as np
from pysdf import SDF

MODELS = {}

# TASK SPECS
GATE_GAP_RANGE = [1.8, 3.0]


def find_roboboat_models():
    configs = glob("./models/*/*.config")
    models = {}
    for path in configs:
        tree = ET.parse(path)
        root = tree.getroot()
        print(root.find("name").text, ":\t", root.find("description").text)
        # Append path to SDF file from config to models dictionary
        models[root.find("name").text] = Path(path).parent / root.find("sdf").text
    return models


def load_sdf(name):
    if name in MODELS:
        return SDF.from_file(MODELS[name])
    else:
        raise Exception(f"No model {name} in found roboboat models")


def load_model(name):
    return load_sdf(name).models[0]


# TODO: this could became a factory
gate_i = 1


def generate_gate(gap=GATE_GAP_RANGE[0], distance=0):
    global gate_i
    rb = load_model("Port Marker Buoy (red)")
    gb = load_model("Starboard Marker Buoy (green)")
    rb.pose.text = f"0 {distance} 0 0 0 0"
    gb.pose.text = f"{gap} {distance} 0 0 0 0"
    rb.name += f" #{gate_i}"
    gb.name += f" #{gate_i}"
    gate_i += 1
    print(gb.pose.value)
    return (rb, gb)


def main():
    sdf = SDF.from_file("./worlds/base.world")
    world = sdf.worlds[0]
    gates = generate_gate()
    world.add(gates)
    gates = generate_gate(distance=7.5)
    world.add(gates)
    print("world:", world)
    print("models:", [m.name for m in sdf.worlds[0].models])
    # print(sdf.worlds[0].to_xml(pretty_print=True))
    sdf.to_file("./worlds/task1-gen.world", pretty_print=True)


if __name__ == "__main__":
    MODELS = find_roboboat_models()
    main()
