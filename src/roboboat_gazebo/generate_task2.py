"""Generate Task 2, from RoboBoat 2023

Specs:
    - gap between gates 1.8m - 3m
    - distance between gates 3m - 6m
    - distance between first and last gate 22.86m - 30.48m
    - starting position, 1.8m away from first gate

Returns:
    Permutations of all spec boundaries, and worlds where [0,0] is ASV starting location
"""

import random
import sys
import xml.etree.ElementTree as ET
from glob import glob
from pathlib import Path

import numpy as np
from pysdf import *

MODELS = {}

# TASK SPECS
BUOY_GAP_RANGE = [1.8, 3.0]

TOTAL_RANGE = [27, 30.48]

GATE_GAP_RANGE = [3.0, 6.0]


def find_roboboat_models():
    configs = glob("./models/*/*.config")
    models = {}
    for path in configs:
        tree = ET.parse(path)
        root = tree.getroot()
        print(root.find("name").text, ":\t", root.find("description").text)
        # Append path to SDF file from config to models dictionary
        models[root.find("name").text] = Path(path).parent / root.find("sdf").text
    return models


def load_sdf(name):
    if name in MODELS:
        return SDF.from_file(MODELS[name])
    else:
        raise Exception(f"No model {name} in found roboboat models")


def load_model(name):
    return load_sdf(name).models[0]


gate_i = 1


def generate_gate(
    gap=BUOY_GAP_RANGE[0], distance=0, obstacle_distance_offset=random.uniform(-1, 1)
):
    global gate_i
    offset = random.uniform(-1, 1)

    rb = load_model("Small gate buoy (red)")
    gb = load_model("Small gate buoy (green)")
    rb.pose.text = f"{offset} {distance} 0 0 0 0"
    gb.pose.text = f"{gap+offset} {distance} 0 0 0 0"
    rb.name += f" #{gate_i}"
    gb.name += f" #{gate_i}"
    gate_i += 1

    obstacle = generate_obstacle(
        distance + obstacle_distance_offset, offset, random.uniform(0 + 0.2, gap - 0.2)
    )

    print(gb.pose.value)

    return (rb, gb, obstacle)


obstacle_i = 1


def generate_obstacle(distance, gap_offset, gap=BUOY_GAP_RANGE[0]):
    global obstacle_i

    black_buoy = load_model("Gate buoy (black)")
    yellow_buoy = load_model("Small gate buoy (yellow)")
    black_buoy.pose.text = f"{gap+gap_offset} {distance} 0 0 0 0"
    yellow_buoy.pose.text = f"{gap+gap_offset} {distance} 0 0 0 0"
    black_buoy.name += f" #{obstacle_i}"
    yellow_buoy.name += f" #{obstacle_i}"
    obstacle_i += 1

    number = random.randint(0, 1)
    if number == 0:
        return black_buoy
    else:
        return yellow_buoy


def main():
    sdf = SDF.from_file("./worlds/base.world")
    world = sdf.worlds[0]
    gates = generate_gate(obstacle_distance_offset=random.uniform(0, 1))
    world.add(gates)

    total_distance = random.uniform(TOTAL_RANGE[0], TOTAL_RANGE[1])

    while True:
        distances = [random.uniform(GATE_GAP_RANGE[0], 4) for i in range(8)]
        distances.append(total_distance - sum(distances))

        if distances[8] < GATE_GAP_RANGE[0] or distances[8] > GATE_GAP_RANGE[1]:
            continue
        else:
            break
    print(distances)
    suma = 0

    for gap in distances:
        suma += gap
        gates = generate_gate(distance=suma)
        world.add(gates)

    print("world:", world)
    print("models:", [m.name for m in sdf.worlds[0].models])
    # print(sdf.worlds[0].to_xml(pretty_print=True))
    sdf.to_file("./worlds/task2-gen.world", pretty_print=True)


if __name__ == "__main__":
    MODELS = find_roboboat_models()
    main()
