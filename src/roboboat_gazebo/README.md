# Helper scripts

Used for world generation. We want to parametrize worlds, so we can test ASV on multiple task variations.

## Getting started

Install Python

```bash
pip install python-sdformat numpy
```

## Running

Run them from repository root.
```bash
# roboboat-world on  main
$ ls
models  README.md  scripts  worlds
$ python ./scripts/generate_task1.py
generated :)
```

Run plot on all world
```bash
for i in worlds/*.world; do [ -f "$i" ] && python scripts/plot_world.py "$i"; done
```

## Side note

We could be using official SDF bindings, but it requires Gazebo Garden. We are using version 9 & 11. We are using simple library and just working on XML tree.
http://sdformat.org/tutorials?tut=python_bindings&cat=developers&
