# RoboBoat Gazebo RoboBoat package
_SDF models and world of RoboBoat for Gazebo 11 and ROS2 Foxy_

> If you want to use ROS1 Noetic see `ros1` branch.

We want to define each task independently, so we can test ASV with given task. Later we want to combine them into a single world.

## Getting started

Dependencies:
* Gazebo 11 and [`gazebo_ros_pkgs`](https://github.com/ros-simulation/gazebo_ros_pkgs/tree/ros2)
* [PX4/PX4-SITL_gazebo-classic: Set of plugins, models and worlds to use with OSRF Gazebo Simulator in SITL and HITL.](https://github.com/PX4/PX4-SITL_gazebo-classic)

For interacting with auto generation scripts, refer to [instructions in scripts folder](./scripts/README.md)

## Installation

Clone to workspace and install with `colcon`.

```bash
colcon build --packages-select roboboat_gazebo
```

## Usage

How to run with minimal Gazebo install, outside docker.

```bash
# we need models from PX4-SITL_gazebo-classic, which is avaiable as a submodule in PX4-Autopilot

# go to folder PX4-Autopilot
cd SeSe/PX4-Autopilot
#run this command
source Tools/simulation/gazebo-classic/setup_gazebo.bash $(pwd) $(pwd)/build/px4_sitl_default
# back to ../SeSe/simulation/src/roboboat_gazebo
#and we point to our models
export GAZEBO_MODEL_PATH=$GAZEBO_MODEL_PATH:$(pwd)/models
#then
gazebo --verbose worlds/course_a.world
# or gzserver
```

Get arguments
```bash
ros2 launch roboboat_gazebo gazebo_ros_launch.py --show-args
```

Select a world
```bash
ros2 launch roboboat_gazebo gazebo_ros_launch.py world:=course_a
```

Where `world:=` refers to world name with given task.

They sometimes might be open in fullscreen mode.

For <include> in .world files to work models need to be added to hidden folder ~/.gazebo/models

### Running SITL

```bash
ros2 launch roboboat_gazebo sese_posix_px4_launch.py
```

## Contributing

We are using SDF 1.7
Consult README files in each directory for specifics.

### Adding models
Each model should have name as defined in https://outline.simle.pl/doc/mapping-simulation-xwtMWkXNhU
We want to create models for each object. So we can reuse them, or link them in world files. If we change anything within a model, we want it reflected in the world without changing much.

### Adding worlds
You can create them using Gazebo editor. But if it contains a lot of objects, generating them would be preffered, especially for task 2.

## Authors and acknowledgment
* Norbert Szulc
* Jakub Wilk
* Wiktoria Piech
* Piotr Stroiński

Thanks to ChatGPT for help creating some of the scripts

## License
GPL v3

## Project status
Development will stop after RoboBoat 2024, unless we will attempt next year competition.
